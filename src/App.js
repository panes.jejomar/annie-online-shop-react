import React, {useState} from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import NavBar from './components/appnavbar';
import Home from './pages/home';
import Register from './pages/register';
import Login from './pages/login';
import Logout from './pages/logout';
import Product from './pages/products';

import { UserProvider } from "./UserContext";
import ProductView from './pages/productView';



export default function App() {

    const [user, setUser] = useState({
      accessToken : localStorage.getItem('accessToken'),
      email : localStorage.getItem("email"),
      isAdmin: localStorage.getItem("isAdmin")=== 'true'
    });

    const unsetUser = () =>{
      localStorage.clear()
    }
    return(
    
    <UserProvider value={{user,setUser,unsetUser}}>
    <Router>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home/>} />
        <Route path="/users/register" element={<Register/>} />
        <Route path="/users/login" element={<Login/>} />
        <Route path="/logout" element={<Logout/>} />
        <Route path="/products" element={<Product/>} />
        <Route path="/products/:productId/product" element={<ProductView />} />
      </Routes>
    </Router>
    </UserProvider>
  )
}
