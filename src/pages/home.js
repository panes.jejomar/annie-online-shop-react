import React from "react";
import { Container } from 'react-bootstrap'
import Carousels from "../components/appcarousel";
import ProductCatalogue from "../components/productsCatalogue"

export default function Home() {
    return (
        <Container>
            <Carousels />
            <ProductCatalogue />
        </Container>
    )
};