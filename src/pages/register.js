import React, { useContext, useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import swal from 'sweetalert2';
import { Form, Button, Col, Row, Card } from "react-bootstrap";
import UserContext from "../UserContext";

export default function Register() {
    const navigate = useNavigate();
    const [name, setName] = useState("")
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);
    const { user } = useContext(UserContext);
    const [contact, setContact] = useState("");
    const [address, setAddress] = useState("");

    useEffect(() => {
        if(
            (name !== "") &&
            (email !== "" && password !== "" && password2 !== "" ) &&
            (password === password2) &&
            (contact !== "") &&
            (address !== "")
        ){
            setIsActive(true)
        } else{
            setIsActive(false)
    }
    }, [name, email, password, password2, contact, address]);

    function registerUser(e){
        e.preventDefault();
        setName("");
        setEmail("");
        setPassword("");
        setPassword2("");
        setContact("");
        setAddress("");

        fetch("https://serene-beyond-65032.herokuapp.com/users/register", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                name: name,
                email: email,
                password: password,
                contact: contact,
                address: address
            })
        }).then((result) => result.json()).then((data) => {
            console.log(data);
             if(data === true){
                swal.fire({
                    title: "Well done!",
                    icon: "success",
                    text: "You have registered successfully"
                });
                navigate('/users/login')
             } else{
                swal.fire({
                    title: "Failed",
                    icon: "error",
                    text: "Failed to register"
                })
             }
        })
    }

    return (

     user.accessToken !== null ? (
        <Navigate to="/users/login" />
    ) : (
        <Row className="middle-content-container">
      <h1 className="text-center m-5">Register</h1>
      <Col md={8} lg={4} sm={8}>
      <Card className="p-4">
          <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Your Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Verify Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Verify Password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Contact</Form.Label>
              <Form.Control
                type="text"
                placeholder="Input contact number"
                value={contact}
                onChange={(e) => setContact(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Input your delivery address"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
              />
            </Form.Group>

            {isActive ? (
              <Button variant="primary" type="submit" className="mt-1">
                Submit
              </Button>
            ) : (
              <Button
                variant="secondary"
                type="submit"
                className="mt-1"
                disabled
              >
                Register
              </Button>
            )}
          </Form>
        </Card>
      </Col>
    </Row>

    )
  )
}