import React,{useEffect,useContext,useState} from "react";
import { Container,Card,Button } from "react-bootstrap";
import UserContext from '../UserContext';
import { useParams,useNavigate,Link } from "react-router-dom";
import Swal from "sweetalert2";


export default function ProductView() {

	const navigate = useNavigate();

	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://serene-beyond-65032.herokuapp.com/products/${productId}/product`)
		.then(res => res.json())
		.then(data => {
			setName(data.pName)
			setDescription(data.pDescription)
			setPrice(data.pPrice)
		})

	} ,)

	const { user } = useContext(UserContext);

	const [pName, setName] = useState('')
	const [pDescription, setDescription] = useState('')
	const [pPrice, setPrice] = useState('')


	//addtoCart function
	const addtoCart = (productId) => {
		fetch(`https://serene-beyond-65032.herokuapp.com/products/addCart/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: 'Successfully Added to Cart',
					icon: 'success'
				})

				navigate('/product')
			}
            else {
				Swal.fire({
					title:`${data.message}`,
					icon: 'error'

				})
			}
		})
	}


	return (
		<Container>
			<Card>
				<Card.Header>
					<h4>{pName}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{pDescription}</Card.Text>
					<h6>Price: Php {pPrice}</h6>
				</Card.Body>

				<Card.Footer>

					{
                        user.accessToken !== null ?
                        <div className="d-grid gap-2">
                            <Button variant="primary" className="col-2" onClick={() => addtoCart(productId)}>
                                Add to Cart
                            </Button>
                        </div>
                        :
                        <Link className="btn btn-warning d-grid gap-2 col-2" to="/login">
                            Login to add to Cart
                        </Link>
                    }
					
				</Card.Footer>
			</Card>
		</Container>

		)
}


