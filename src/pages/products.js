import React, { useState,useContext,useEffect } from "react";
import UserContext from "../UserContext";
import ProductAdminView from "../components/adminView";
import ProductUserView from "../components/userView";

export default function Product(){
    const {user} = useContext(UserContext);
    const [allProducts, setAllProducts] = useState([]);

    const fetchData = () => {
        <> {user.isAdmin === true ? ( 
          fetch("https://serene-beyond-65032.herokuapp.com/products/allproducts", {
            headers: {
              Authorization: `Bearer ${user.accessToken}`
            }
          }).then((result) => result.json()).then((data) => {
                setAllProducts(data);
                console.log(data)
          })
          )
          :
          ( 
            fetch("https://serene-beyond-65032.herokuapp.com/products/", {
              headers: {
                Authorization: `Bearer ${user.accessToken}`
              }
            }).then((result) => result.json()).then((data) => {
                  setAllProducts(data);
                  console.log(data)
            })
            )
          }
        </>
          
      };
      useEffect(() => {
        fetchData();
      },[]);

      return (
        <React.Fragment>
          {user.isAdmin === true ? (
            <ProductAdminView productData={allProducts} fetchData={fetchData} />
          ) : (
            <ProductUserView productData={allProducts} />
          )}
        </React.Fragment>
      );
}