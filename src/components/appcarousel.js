import React from "react";
import { Carousel } from "react-bootstrap";
import 'holderjs';
import '../App.css'


export default function Carousels() {
  return (
    <>
      <Carousel>
        <Carousel.Item interval={5000}>
          <img
            className="d-block mx-auto car-image"
            src="https://images.pexels.com/photos/3735209/pexels-photo-3735209.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3 className="car-capt">Fresh from Soya farm in Batanes</h3>
            
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={5000}>
          <img
            className="d-block mx-auto car-image"
            src="https://images.pexels.com/photos/3735169/pexels-photo-3735169.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
            alt="Second slide"
          />
          <Carousel.Caption>
            <h3 className="car-capt">10% for members</h3>
         
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={5000}>
          <img
            className="d-block mx-auto car-image"
            src="https://images.pexels.com/photos/3735163/pexels-photo-3735163.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3 className="car-capt">Get a chance to win a trip to Batanes</h3>
          
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </>
  );
}
