import React, { useEffect, useState } from "react";
import ProductCard from "./appproductscard";

export default function ProductUserView({productData}) {
    const [products, setProducts] = useState([]);
    useEffect(() => {
        const productArr = productData.map(product => {
            if(product.pIsActive === true) {
                return (
                    <ProductCard productData={product} key={product._id} />
                ) 
            } else {
                return null;
            }
        })
        setProducts(productArr)
    }, [productData])
    
    return(
        <>
        {products}
        </>
     )
}