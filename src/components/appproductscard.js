import React, {useContext} from "react";
import {Button, Card} from 'react-bootstrap';
import UserContext from "../UserContext";
import { useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

export default function ProductCard({productData}) {
    const navigate = useNavigate();
    const {user} = useContext(UserContext);
    const {pName, pDescription, pPrice,_id} = productData;

    const addToCart = (productId) => {
        fetch("https://serene-beyond-65032.herokuapp.com/products/", {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem(`accessToken`)}`
            },
        }).then(res => res.json()).then(data => {
            if(data === true){
                Swal.fire({
                    title: 'Successfully placed an order',
                    icon: 'success'
                })
                navigate('/products')
            } else {
                Swal.fire({
                    title: 'Failed to place an order',
                    icon: 'error'
                })
            }
        })
    }

    return(
        <>
        <Card className="mt-2">
        <Card.Body>
          <Card.Title>{pName}</Card.Title>

          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{pDescription}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {pPrice} </Card.Text>
          
         
          {user.accessToken !== null ?
            <div className="d-grid gap-2">
                <Button variant="primary" className="col-2" onClick={() => addToCart(_id)}>
                    Add to Cart
                </Button>
            </div>
                :
            <Link className="btn btn-warning d-grid gap-2 col-2" to="/users/login">
                Login to add to Cart
            </Link>
            }

        </Card.Body>
         </Card>
        </>

    )
}