import React, {useState} from "react";
import {Modal, Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Addproduct({fetchData}) {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [showAdd, setShowAdd] = useState(false);

    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);

    const Addproduct = (e) => {
        e.preventDefault();

        fetch('https://serene-beyond-65032.herokuapp.com/products/create',  {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json()).then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'You have added a new product'
                })
                closeAdd();
                fetchData();
            } else{
                Swal.fire({
                    title: 'Oops! Something went wrong',
                    icon: 'error',
                    text: "Please try again."
                })
                closeAdd();
                fetchData();
            }

            //clear inpput fields
            setName('');
            setDescription('')
            setPrice('');
        })
    }

    return(
    <>
        <Button variant='primary' onClick={openAdd}>Add new Product</Button>
        <Modal show={showAdd} onHide={closeAdd}>
            <Form onSubmit={(e) => Addproduct(e)}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Product</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" values={name} onChange={e => setName(e.target.value)} required />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" values={description} onChange={e => setDescription(e.target.value)} required />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" values={price} onChange={e => setPrice(e.target.value)} required />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant='secondary' onClick={closeAdd}>Close</Button>
                    <Button variant='success' type="submit">Submit</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    </>
    )
};