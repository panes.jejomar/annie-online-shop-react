import React from "react";
import { Button, ButtonToolbar, Overlay, OverlayTrigger, Popover } from "react-bootstrap";
import Archiveproduct from "./archiveProduct";
import Updateproduct from "./updateProduct";

export default function Popoverbutton(props) {
    const {productData, fetchData, pIsActive} = props;
    
    const popover = (
        <Popover id="popover-basic">
            <Updateproduct fetchData={fetchData} product={productData} />
            <Archiveproduct fetchData={fetchData} productId={productData} pIsActive={pIsActive} />
        </Popover>
    );

    return(
        <>
        <ButtonToolbar>
          <OverlayTrigger trigger="click"  rootCloseEvent="mousedown" placement="left" overlay={popover}>
            <Button><i className="fa fa-bars"></i></Button>
          </OverlayTrigger>
         </ButtonToolbar>
        </>
      
    )

}