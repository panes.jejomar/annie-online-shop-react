import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import '../App.css'
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';

export default function NavBar() {
    const {user} = useContext(UserContext);
    return(
        <Navbar expand="lg" variant="light" id="nav-bar">
            <Navbar.Brand className="ms-5" id="brand" >Annie's Shop</Navbar.Brand>
            <Navbar.Toggle href aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto me-5" id="nav">
                <Nav.Link as={Link} to="/" className='nb-link' id="h-id">Home</Nav.Link>
                <Nav.Link as={Link} to="/products" className='nb-link' id="p-id">Products</Nav.Link>
                { (user.accessToken === null) ? 
                    <>
                        <Nav.Link as={Link} to="/users/login" className='nb-link' id="li-id">Login</Nav.Link>
                        <Nav.Link as={Link} to="/users/register" className='nb-link' id="r-id">Register</Nav.Link>
                    </>:
                    <Nav.Link as={Link} to="/logout" className='nb-link' id="lo-id">Logout</Nav.Link>
                }
            </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
