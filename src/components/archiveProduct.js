import React from "react";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";


export default function ArchiveProduct({productId, pIsActive, fetchData}){

    const archiveToggle = (productId) => {
		fetch(`https://serene-beyond-65032.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		}).then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: `Error`,
					icon: 'Error',
					text: 'Failed to deactivate'
					
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully deactivated'
				})
				fetchData();
			}


		})
	}

	const unarchiveToggle = (productId) => {
		fetch(`https://serene-beyond-65032.herokuapp.com/products/${productId}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		}).then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: `${data.message}`,
					icon: 'Error',
					text: 'Failed to reactivate'
					
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully reactivated'
				})
				fetchData();
			}


		})
	}

    return (<>
	{pIsActive ?
           
		   <Button variant="outline-warning"  className="over-buttons" onClick={() => archiveToggle(productId)}><i className="fas fa-archive"></i>Archive</Button>
		   :
		   <Button variant="outline-success" className="over-buttons" onClick={() => unarchiveToggle(productId)}><i className="fas fa-redo"></i> Restore</Button>

	   }

	</>)

}
