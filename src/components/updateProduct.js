import React, {useState} from "react";
import {Modal, Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function Updateproduct({fetchData, product}) {
    const [productId, setproductId] = useState('');
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [showUpdate, setShowUpdate] = useState(false);

    const openUpdate = (productId) => {
        fetch(`https://serene-beyond-65032.herokuapp.com/products/product/${productId}`)
        .then(res => res.json())
        .then(data => {
            setproductId(data._id);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price)
        })
        setShowUpdate(true);
    }
    const closeUpdate = () => {
        setShowUpdate(false);
        setName('');
        setDescription('');
        setPrice(0);
    }

    const updateproduct = (e, productId) => {
        e.preventDefault();

        fetch(`https://serene-beyond-65032.herokuapp.com/products/update/${productId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then((data) => {
            if(data !== true){
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Update completed'
                })
                closeUpdate();
                fetchData();
            }
            else {
                Swal.fire({
                    title: "Failed",
                    icon: 'error',
                    text: 'Failed to update'
                })
                closeUpdate();
                fetchData();
            }
        })
    }

    return(
    <>
        <Button variant="outline-primary" className="over-buttons" onClick={() => openUpdate(product)}>
            Edit
        </Button>

        <Modal show={showUpdate} onHide={closeUpdate} data-backdrop='static'>
            <Form onSubmit={e => updateproduct(e, productId)}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Product</Modal.Title>
                </Modal.Header>
                
                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required></Form.Control>
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant='secondary' onClick={closeUpdate}>Close</Button>
                    <Button variant="success" type="submit">Submit</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    </>
    )

}