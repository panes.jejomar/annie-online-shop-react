import React from "react";
import { Col ,Card } from "react-bootstrap";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

export default function CatalougueCard({ productData }) {
  const {_id, pName, pPrice, pDescription } = productData;
  return (
    <Col lg={3} md={6} sm={6}>
        <Card className="m-2 card-min-height h-100"  >
        <Card.Header id="card-header"><b>{pName}</b></Card.Header>
          <Card.Body>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{pDescription}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>Php {pPrice} </Card.Text>
            <Link className="btn btn-primary" to={`/products/${_id}/product`}>View Product</Link>
          </Card.Body>
        </Card>
    </Col>
  );
}
CatalougueCard.propTypes = {
	productData: PropTypes.shape({
		pName: PropTypes.string.isRequired,
		pDescription: PropTypes.string.isRequired,
		pPrice: PropTypes.number.isRequired
	})
}

