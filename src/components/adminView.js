import React, {useEffect, useState} from "react";
import {Container, Table} from "react-bootstrap";
import Popoverbutton from "./apppopover";
import Addproduct from "./addProduct";


export default function ProductAdminView(props) {
    const {productData, fetchData} = props;
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const productArr = productData.map((product) => {
            return(
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.pName}</td>
                    <td>{product.pDescription}</td>
                    <td>{product.pPrice}</td>
                    <td className={product.pIsActive ? "text-success" : "text-danger"}>
                        {product.pIsActive ? "Available" : "Unavailable"}
                    </td>
                    <td>
                        <Popoverbutton productData = {product._id} fetchData={fetchData} pIsActive={product.pIsActive} />
                    </td>
                </tr>
            )
        });
        setProducts(productArr);
    }, [productData]);

    return(
        <Container>
            <div className="text-center-my-4">
                <h1>Admin Dashboard</h1>
                <Addproduct fetchData={fetchData} />
            </div>

            <Table id="ProductDataTable">
                <thead striped="true" bordered="true" hover="True" responsive ="true">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Availabilty</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>{products}</tbody>
            </Table>
        </Container>
    );
}